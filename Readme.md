## About this project

This is the final project for the course **M101JS: MongoDB for Node.js Developers**
from  [MongoDB University](https://university.mongodb.com/)
Which I completed in Feb, 2017

The project exercised the various aspects of materials covered during the whole course including
Schema Design, Indexes and Performance, Aggregation framework (hardest topic to grasp, for me), etc.

### How to run.

1. Install the dependencies. Run `npm install`
2. Make sure you have a mongod running version 3.2.x of MongoDB or above.
3. Import the "item" collection: `mongoimport --drop -d mongomart -c item data/items.json`
4. Import the "cart" collection: `mongoimport --drop -d mongomart -c cart data/cart.json`
5. Run the application by typing `node mongomart.js`

Point your browser to `localhost:3000` to see the live app.
